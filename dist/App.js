import React from "react";
import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";
function Home() {
  return (
    <div>
      <h1>test wepback test home</h1>
      <a href="{% url 'djangoapp:about' %}">About</a>
    </div>
  );
}
function About() {
  return (
    <div>
      <h1>test wepback cuhuytien test about</h1>
      <a href="{% url 'djangoapp:about' %}">About</a>
    </div>
  );
}
export default function App() {
  return (
    <Router>
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about">About</Link>
          </li>
        </ul>
      </nav>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/about" element={<About />}></Route>
      </Routes>
    </Router>
  );
}
